# Integrasi Composite Simpson untuk menghitung probabilitas antar dua z-score
# Input: a (batas bawah integrasi), b (batas atas integrasi), tol (toleransi)
# Output: S (integral fungsi f pada interval [a,b])
function S = compsimpson(a,b,TOL)
  tic
  % Definisikan fungsinya
  f = @(x) (e^(-x^2/2))/sqrt(2*pi);

  % Several variables needed for computation
  N = 1;  % To remember divider of h in an iteration
  j = 0;  % Power of N (N = 2^j)

  % stepsize, subdivided in half until successive approximations within TOL
  h = (b-a)/2;

  % Compute initial S0 and S to compare
  S0 = 0;
  S = f(h)*(b-a);
  while(abs(S-S0) > TOL)
    S0 = S;
    h = (b-a)/(2*N);
    i = 0:N-1;

    % Do computation on 3 points
    xi = a + 2*i*h;
    xi1 = a + 2*(i+0.5)*h;
    xi2 = a + 2*(i+1)*h;

    sum = 0;
    for i=1:length(xi)
      sum = sum + (f(xi(i)) +4*f(xi1(i)) + f(xi2(i)));
    endfor

    S = (h/3)*sum;
    j = j+1;
    N = 2^j;
  endwhile
  toc
endfunction
