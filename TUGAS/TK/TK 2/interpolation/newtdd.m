%Program 3.1 Newton Divided Difference Interpolation Method
%Computes coefficients of interpolating polynomial
%Input: x and y are vectors containing the x and y coordinates
% of the n data points
%Output: coefficients c of interpolating polynomial in nested form
%Use with nest.m to evaluate interpolating polynomial
function solusi=newtdd(x,y,n)
   % initialize
  A = zeros(n, n);
  A(:,1) = ones(n,1);

  % Membentuk matrix
  for i = 2:n
    curr = 1;
    for j = 2:i
      A(i, j) = A(i, j-1)*(x(i) - x(curr));
    curr=curr+1;
  end
  solusi = forward(A,y);
end

endfunction
