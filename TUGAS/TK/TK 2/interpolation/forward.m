function x = forward(L,b)
  n = length(b);
  x = zeros(n,1);
  x(1) = b(1)/L(1,1);
  for i = 2:n
    sum = 0;
    for j = 1 : i-1
      sum = sum + x(j)*L(i,j);
    endfor
    x(i) = (b(i)-sum)/L(i,i);
  endfor
end
