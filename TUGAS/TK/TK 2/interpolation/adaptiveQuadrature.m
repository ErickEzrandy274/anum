function area = adaptiveQuadrature(a,b,TOL)
  tic
  % Initialize area
  area = 0;

  % Define the function
  f = @(x) (e^(-x^2/2))/sqrt(2*pi);

  % Define several variables for computing Simpson integration
  h1 = (b-a)/2;
  x1 = (a:h1:b);
  w1 = [1 4 1];
  h2 = h1/2;
  x2 = (a:h2:b);
  w2 = [1 4 2 4 1];

  % Compute estimate1 using Simspon integration
  estimateSum = 0;
  for i = 1:3
    estimateSum = estimateSum + f(x1(i))*w1(i);
  endfor
  estimate1 = h1/3*estimateSum;

  % Compute estimate2 using Simspon integration
  estimateSum = 0;
  for i = 1:5
    estimateSum = estimateSum + f(x2(i))*w2(i);
  endfor
  estimate2 = h2/3*estimateSum;

  % Compute the error
  err = abs(estimate1-estimate2)/15;

  if err < TOL
    area = estimate2;
  else
    leftArea = adaptiveQuadrature(a,(a+b)/2,TOL/2);
    rightArea = adaptiveQuadrature((a+b)/2,b,TOL/2);
    area = leftArea + rightArea;
  endif
  toc
endfunction
