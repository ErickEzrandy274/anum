# taken from https://www.geeksforgeeks.org/sparse-matrix-representation/
class Sparse:
    # Making the slots for storing row,
    # column, value, and address
    __slots__ = "row", "col", "data", "next"
 
    # Constructor to initialize the values
    def __init__(self, row=0, col=0, data=0, next=None):
        self.row = row
        self.col = col
        self.data = data
        self.next = next

    def insertChild(self, newNode):
        prevNode = self
        currNode = self.next

        while currNode != None:
            if (newNode.col < currNode.col):
                prevNode.next = newNode
                newNode.next = currNode
                break
            else:
                prevNode = currNode
                currNode = currNode.next
        
        if currNode == None:
            prevNode.next = newNode
 
    def PrintList(self):
        temp = self
        print("row_position:", end=" ")
        while temp != None:
            print(temp.row, end=" ")
            temp = temp.next
        print()

        temp = self
        print("column_postion:", end=" ")
        while temp != None:
            print(temp.col, end=" ")
            temp = temp.next
        print()

        temp = self
        print("Value:", end=" ")
        while temp != None:
            print(temp.data, end=" ")
            temp = temp.next
        print()