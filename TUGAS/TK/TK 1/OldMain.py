def main():
    x_real, b, row, col, data = parseYYY()

    # x_real  = np.array([1, 2, 1, 3, 1])
    # b  = np.array([0.3300, 0.5095, 4.9215, 0.1188, 1.3113])
    # row  = np.array([3, 2, 4, 1, 0, 2, 0, 2])
    # col  = np.array([0, 1, 1, 2, 3, 3, 4, 4])
    # data = np.array([0.118818, 0.729234, 0.655655, 0.509511, 0.093481, 0.952264, 0.049543, 0.606262])
    # var = sparse.coo_matrix((data, (row, col)),shape=(70,70))

    # Creating Object
    dim = 100
    sparseParent = SparseParent(dim)
    start_time = time.time()
    for i in range(row.size):
        # Creating Linked List by only those elements which are non-zero
        sparseParent.insert(row[i], col[i], data[i])
    
    L, U, p, bt = SparseLUFactwithPivot(sparseParent, b)

    # testL = [ [0] * 5 for _ in range(5)]
    # for i in range(5):
    #     currentN = L.collection[i]
    #     while (currentN != None):
    #         testL[i][currentN.col] = currentN.data
    #         currentN = currentN.next

    # testU = [ [0] * 5 for _ in range(5)]
    # for i in range(5):
    #     currentN = U.collection[i]
    #     while (currentN != None):
    #         testU[i][currentN.col] = currentN.data
    #         currentN = currentN.next

    # print(f"L: {L}")
    # print(f"U: {U}")
    # print(f"p: {p}")
    # print(f"bt: {bt}")
    # print(f"test L:\n {testL[0]} \n {testL[1]} \n {testL[2]} \n {testL[3]} \n {testL[4]}")
    # print(f"test U:\n {testU[0]} \n {testU[1]} \n {testU[2]} \n {testU[3]} \n {testU[4]}")

    y = Forward(L, bt)

    count = 0
    for i in y:
        y[count] = roundd(i)
        count += 1


    # print(f"test U:\n {testU[0]} \n {testU[1]} \n {testU[2]} \n {testU[3]} \n {testU[4]}")
    x = Backward(U, y)

    # print(f"x: {x}")
    # print(f"x_real: {x_real}")
    print(f"Execution time: {(time.time() - start_time)} seconds")