from Sparse import *

class SparseParent:
    # Initialize Class Variables
    def __init__(self, row):
        # list of parent Sparse per row
        self.collection = [None for i in range(row)]
        self.row = row
 
    # Function which returns the size of the Linked List
    def size(self):
        return self.row
 
    def isEmpty(self, row):
        return self.collection[row] == None
 
    # Responsible function to create Linked List from Sparse Matrix
    def insert(self, row, col, data):
        newNode = Sparse(row, col, data, None)

        if self.isEmpty(row):
            self.collection[row] = newNode
        else:
            parent = self.collection[row]
            if parent.col > newNode.col:
                self.collection[row] = newNode
                newNode.next = parent
            else:
                parent.insertChild(newNode)

    def delete(self, prevNode, currentNode, row):
        if (prevNode == None):
            self.collection[row] = currentNode.next
        else:
            prevNode.next = currentNode.next

    def searchMaxValueByColumn(self, startBaris, maxKolom):
        maxValue = 0
        maxRow = -1
        for i in range(startBaris, self.size()):
            currentNode = self.collection[i]
            while currentNode != None and currentNode.col <= maxKolom:
                if (currentNode.col == maxKolom):
                    if (abs(currentNode.data)) > abs(maxValue):
                        maxValue = currentNode.data
                        maxRow = i
                currentNode = currentNode.next
        
        return [maxValue, maxRow]

    def getRatio(self, baris, kolom, diagElement):
        ratio = 0
        currentNode = self.collection[baris]

        while currentNode != None and currentNode.col <= kolom:
            if (currentNode.col == kolom and currentNode.data != 0):
                ratio = currentNode.data/diagElement
                break

            currentNode = currentNode.next

        return ratio

    def compute(self, baris, kolom, ratio):
        baseNode = self.collection[kolom]
        currentNode = self.collection[baris]
        prevNode = None
        while (baseNode != None and currentNode != None):
            if (currentNode.col == baseNode.col):
                currentNode.data -= ratio * baseNode.data
                baseNode = baseNode.next

                # Mengubah nilai pada kolom iterasi menjadi 0, mengurangi propagasi error
                if (currentNode.col == kolom):
                    currentNode.data = 0
                if (currentNode.data == 0):
                    self.delete(prevNode, currentNode, baris)
                    currentNode = currentNode.next
            else:
                if (currentNode.col < baseNode.col):
                    prevNode = currentNode
                    currentNode = currentNode.next
                else:
                    self.insert(baris, baseNode.col, -1 * ratio * baseNode.data)
                    baseNode = baseNode.next
        while (currentNode == None and baseNode != None):
            self.insert(baris, baseNode.col, -1 * ratio * baseNode.data)
            baseNode = baseNode.next

    def printList(self):
        for sp in self.collection:
            if (sp == None):
                continue
            sp.PrintList()