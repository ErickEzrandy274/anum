# taken from https://www.geeksforgeeks.org/sparse-matrix-representation/
import time
import scipy.sparse as sparse
import numpy as np
import scipy.io as sio
from SparseParent import *
from Sparse import *
from FromOctave import *
import tracemalloc

def roundd(x):
    # return round(x, 4)
    return round(x, 2)


def SparseLUFactwithPivot(A: SparseParent, b):
    n = A.size()

    # matriks L Tanpa diagonal utama bernilai satu
    LSparseParent = SparseParent(n)
    a = np.arange(1, n+1, 1)

    #jalan kolom
    for k in range(n-1):
        # Pivoting
        x, m = A.searchMaxValueByColumn(k, k)

        
        A.collection[k], A.collection[m] = A.collection[m], A.collection[k]
        
        a[k], a[m] = a[m], a[k]

        LSparseParent.collection[k], LSparseParent.collection[m] = LSparseParent.collection[m], LSparseParent.collection[k]

        b[k], b[m] = b[m], b[k]


        # jalan baris
        for i in range(k+1, n):
            ratio = A.getRatio(i, k, x)
            if ratio != 0:
                LSparseParent.insert(i, k, ratio)
                A.compute(i, k, ratio)

        # testA = [ [0] * 5 for _ in range(5)]
        # for i in range(5):
        #     currentN = A.collection[i]
        #     while (currentN != None):
        #         testA[i][currentN.col] = currentN.data
        #         currentN = currentN.next
                
    # Penambahan diagonal utama matriks L dengan satu
    for i in range(n):
        LSparseParent.insert(i, i, 1)
        
    return [LSparseParent, A, a, b]

def Forward(L: SparseParent, bt): 
    n = L.size()
    y = np.zeros(n)
    y[0] = bt[0]/L.collection[0].data

    for i in range(1, n):   
        sum = 0  
        currentNode = L.collection[i]
        while currentNode != None and currentNode.col <= i:
            sum += y[currentNode.col] * currentNode.data
            if (currentNode.col == i):
                break
            currentNode = currentNode.next

        y[i] = (bt[i] - sum) / currentNode.data
    return y

def Backward(U: SparseParent, y):
    n = U.size()
    x = np.zeros(n)
    x[n-1] = y[n-1]/U.collection[n-1].data
    
    for i in range(n-2, -1, -1): 
        sum = 0
        headNode = U.collection[i]

        if headNode.col != i:
            headNode = headNode.next
        
        if U.collection[i].data == 0:
            continue

        # print(f"head U pada {i}: {headNode.data}")
        # print(f"y pada {i}: {y[i]}")
        currentNode = headNode.next

        while(currentNode != None):
            sum += x[currentNode.col] * currentNode.data
            currentNode = currentNode.next
        
        x[i] = (y[i] - sum) / headNode.data
    return x

#  input unsorted
def main():
    x_real, b, row, col, data = parseYYY()

    # Creating Object
    dim = 300
    sparseParent = SparseParent(dim)

    for i in range(row.size):
        # Creating Linked List by only those elements which are non-zero
        sparseParent.insert(row[i], col[i], data[i])

    compute(row, col, data, b, sparseParent)
    # for i in range(row.size):
    #     # Creating Linked List by only those elements which are non-zero
    #     sparseParent.insert(row[i], col[i], data[i])
    
    # L, U, p, bt = SparseLUFactwithPivot(sparseParent, b)
    # y = Forward(L, bt)

    # count = 0
    # for i in y:
    #     y[count] = roundd(i)
    #     count += 1

    # x = Backward(U, y)
 
    # stopping the library

def compute(row, col, data, b, sparseParent):
    tracemalloc.start()
    start_time = time.time()
    
    L, U, p, bt = SparseLUFactwithPivot(sparseParent, b)
    y = Forward(L, bt)

    count = 0
    for i in y:
        y[count] = roundd(i)
        count += 1

    x = Backward(U, y)
    print(f"Execution time: {(time.time() - start_time)} seconds")
    print(tracemalloc.get_traced_memory())
    tracemalloc.stop()

if __name__ == "__main__":
    main()