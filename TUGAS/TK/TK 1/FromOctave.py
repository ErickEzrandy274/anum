import scipy.io as sio
import numpy as np

# https://docs.scipy.org/doc/scipy/tutorial/io.html

def forXXX(raw):
  x = []
  for i in raw:
    x.append(i[0])

  return x

def forXXX1(raw):
  x = []
  for i in raw:
    x.append(int(i[0]))
  return x

def forXXX2(raw):
  x = []
  for i in raw:
    x.append(int(i[0]) - 1)
  return x

def parseYYY():
#   mat_contents = sio.loadmat('./Eksperiment/70/70x70x25.mat')
  mat_contents = sio.loadmat('./Eksperiment/300/300x300x10.mat')

  x_real_raw  = mat_contents['x_real']
  b_raw  = mat_contents['b']
  row_raw = mat_contents['row']
  col_raw = mat_contents['col']
  data_raw = mat_contents['data']

  x_real = forXXX(x_real_raw)
  b = forXXX(b_raw)
  row = forXXX2(row_raw)
  col = forXXX2(col_raw)
  data = forXXX(data_raw)

  return np.array(x_real), np.array(b), np.array(row), np.array(col), np.array(data)