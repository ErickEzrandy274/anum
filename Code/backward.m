function x = backward(U,b)
  n = length(b);
  x = zeros(n,1);
  x(n) = b(n)/U(n,n);
  for i = n-1:-1:1
    sum = 0;
    for j = n : -1 : i+1 # alternatif lain ->i+1 : n
      sum = sum + x(j)*U(i,j);
    endfor
    x(i) = (b(i)-sum)/U(i,i);
  endfor
end
