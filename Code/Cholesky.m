function [G] = Cholesky(A)
  [n,n] = size(A);
  G(1,1) = sqrt(A(1,1));
  #kolom
  for k = 1 : n-1
    #baris
    for i = k + 1 : n
      if (k==1)
        G(i,k) = A(i,k)/G(k,k);
      else
        G(i,k) = (A(i,k) - (G(i, 1:k-1)*G(k,1:k-1)))/G(k,k);
    endfor
    G(k+1,k+1) = sqrt(A(k+1,k+1)-G(k+1,1:k)*G(k+1,1:k)');
  endfor


