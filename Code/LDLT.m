function [L,D] = LDLT(A)
  [n,n] = size(A);
  L = eye(n);
  D = zeros(n);

  #jalan kolom
  for j = 1:n-1
    D(j,j) = A(j,j);
    #baris
    for i = j+1 : n
       L(i,j) = A(j,i)/A(j,j);
       A(i,j:n) = A(i,j:n) - L(i,j)*A(j,j:n);
    endfor
  endfor
  D(n,n) = A(n,n);

