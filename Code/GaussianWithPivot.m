function [U,bt,p] = GaussianwithPivot(A,b)
  n = length(b);
  A = [A b]; #matrix augmented
  p = 1:n;

  #jalan kolom
  for j = 1:n-1
    [x,m] = max(abs(A(j:n,j))); #k:n -> baris (dr diagonal ke bwh)
    #baris
    m=m+(j-1);
    A([j m],:) = A([m j],:);
    p([j m]) = p([m j]);
    for i = j+1 : n
      m = A(i,j)/A(j,j); #ratio
      A(i,:) = A(i,:) - m*A(j,:); #for implisit
    endfor
  endfor
  U= A(1:n,1:n);
  bt = A(:, n+1);

#flopsnya: n^3 (ada 3 for)

