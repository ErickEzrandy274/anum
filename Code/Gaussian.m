function [U,bt] = Gaussian(A,b)
  n = length(b);
  A = [A b]; #matrix augmented

  #jalan kolom
  for k = 1:n-1
    #baris
    for i = k+1 : n
      m = A(i,k)/A(k,k); #ratio
      A(i,:) = A(i,:) - m*A(k,:); #for implisit
    endfor
  endfor
  U= A(1:n,1:n);
  bt = A(:, n+1);

#flopsnya: n^3 (ada 3 for)
