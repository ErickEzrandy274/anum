function [L,U] = BandedLU(A,p,q)
  [n,n] = size(A);
  L = eye(n);
  counter = 0;

  #jalan kolom
  for k = 1:n-1
    #baris
    for i = k+1 : k+q
      L(i,k) = (A(i,k)/A(k,k));
      A(i,k:min(k+p,n)) = A(i,k:min(k+p,n)) - L(i,k)*A(k,k:min(k+p,n));
      counter = counter + 1;
    endfor
  endfor
  U = A;
  counter

