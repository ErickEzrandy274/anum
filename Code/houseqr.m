function [q,R] = houseqr(A)
  [m,n] = size(A);
  q = eye(m,m);

  for k = 1:min(n,m-1)
    x = A(i:m,i);
    w = [-sign(x(1))*norm(x);zeros(m-i,1)];
    v = w-x;
    h = eye(m,m);
    h(i:m,i:m) = eye(m-i+1, m-i+1) - 2*v*v' / (v'*v);
    q = q*h;
    a = h*a;
  endfor
    R = a;
end
