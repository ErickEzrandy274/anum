function [L,D, p] = LDLTwithPivot(A)
  [n,n] = size(A);
  L = eye(n);
  D = zeros(n);
  p = 1 : n;

  #jalan kolom
  for j = 1:n-1
    [x,m] = max(abs(diag(A(j:n,j:n))));
    m = m + (j-1);
    A([j m],:) = A([m j],:);
    A(:,[j m]) = A(:, [m j]);
    p([j m]) = p([m j]);
    L([j m],1:j-1) = L([m j],1:j-1);
    D(j,j) = A(j,j);
    #baris
    for i = j+1 : n
      L(i,j) = A(j,i)/A(j,j);
      A(i,j:n) = A(i, j:n) - L(i,j)*A(j,j:n); #membuat submatrix baru
    endfor
  endfor
  D(n,n) = A(n,n);

