function [L,U] = LUFact(A)
  [n,n] = size(A);
  L = eye(n);

  #jalan kolom
  for k = 1:n-1
    #baris
    for i = k+1 : n
      m = (A(i,k)/A(k,k));
      L(i,k) = m;
      A(i,:) = A(i,:) - m * A(k,:);
    endfor
  endfor
  U= A(1:n,1:n);

