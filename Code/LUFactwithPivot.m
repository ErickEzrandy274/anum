function [L,U,p,b] = LUFactwithPivot(A,b)
  [n,n] = size(A);
  A = [A b];
  L = eye(n);
  p=1:n;

  #jalan kolom
  for k = 1:n-1
    [x,m] = max(abs(A(k:n,k)));
    m = m + (k-1);
    A([k m],:) = A([m k],:);
    p([k m]) = p([m k]);
    L([k m],1:k-1) = L([m k],1:k-1);
    #baris
    for i = k+1 : n
        r = (A(i,k)/A(k,k));
        L(i,k) = r;
        A(i,1:n) = A(i,1:n) - r * A(k,1:n);
    endfor
  endfor
  U= A(1:n,1:n);
  b= A(:,n+1);
